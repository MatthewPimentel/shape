/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matth
 */
public class Circle extends Shape {

    public Circle(double d) {
        super(d);
    }
    
    public double getArea(double d){
        return Math.PI * Math.pow(d, 2);
    }
    
    public double getPerimeter(double d){
        return 2 * Math.PI * d;
    }
}
