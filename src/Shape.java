/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matth
 */
public abstract class Shape {
    private double d;

    public Shape(double d) {
        this.d = d;
    }
    
    
    
    abstract double getArea(double d);
    abstract double getPerimeter(double d);

    
}
