/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matth
 */
public class ShapeSimulation {
    public static void main(String [] args){
        Circle circle = new Circle(8);
        Square square = new Square(6);
        
        System.out.println(circle.getArea(8));
        System.out.println(circle.getPerimeter(8));
        System.out.println(square.getArea(6));
        System.out.println(square.getPerimeter(6));
    }
}
