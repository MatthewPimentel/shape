/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matth
 */
public class Square extends Shape {

    public Square(double d) {
        super(d);
    }

    @Override
    public double getArea(double d) {
        return d * d;
    }

    @Override
    double getPerimeter(double d) {
        return 4*d;
    }
    
    
}
